## How to start in JupyterHub?

1. Log in to [JupyterHub](https://naf-jhub.desy.de/hub/login)
2. For Jupyter Launch Modus, select: **Launch JupyterLAB**
3. From the opened window launch a Terminal
4. Clone this repository to your home directory (you are in your home after launching the terminal) by calling ```git clone https://gitlab.cern.ch/cms-podas23/topical/pytorch-tutorial.git```
5. Go into the repository by calling ```cd pytorch-tutorial```
6. Run ```sh link_kernel.sh```
7. In the file tree on the left, open the directory **pytorch-tutorial** and run the **pytorch_tutorial.ipynb** notebook

**Only in case nothing works**

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fcollaborating.tuhh.de%2Fl_stz%2Fpytorch-tutorial/master)
